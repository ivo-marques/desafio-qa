#language: pt-br
Funcionalidade: Criar Grupo
     Para quando eu acessar o whatsapp para iOS
     Eu, como usuário
     Desejo criar um novo grupo

Contexto: Usuário com Whatsapp para iOS
     Dado que sou usuário
     E acesso o Whatsapp
     E acesso a guia conversas

Cenário: Criar um novo grupo no whatsapp
     Quando toco na palavra "Novo Grupo"
     E seleciono um ou mais contatos
     E toco a palavra "seguinte"
     E preencho o nome do grupo
     E toco na palavra "criar"
     Então o novo grupo é criado
     E a tela de conversa para o grupo é exibida