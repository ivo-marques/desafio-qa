package br.com.ivomarques.checkout;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Checkout {

	private Map<String,Integer> productList;
	private Map<String, Rule> rules;
	
	public Checkout(Rule rules[]) {
		this.rules = new HashMap<String,Rule>();
		for (Rule rule : rules) {
			this.rules.put(rule.getProduct(), rule);
		}
		this.productList = new HashMap<String, Integer>();
	}
	
	public boolean scan(String product){
		Rule rule = rules.get(product);
		if (rule != null) {
			Integer quant = productList.get(product);
			if (quant == null) {
				quant = 0;
			} else {
				productList.remove(product);
			}
			productList.put(product, ++quant);
			return true;
		}
		return false;
	}
	
	public Double total() {
		Double total = 0.0;
		for (Entry<String, Integer> entry : productList.entrySet()) {
			Integer quant = entry.getValue();
			Double itemTotal = 0.0;
			Rule rule = rules.get(entry.getKey());
			SpecialPrice special = rule.getSpecialPrice();
			if (special != null && quant > special.getQuant()) {
				itemTotal += (quant / special.getQuant()) * special.getPrice();
				quant = quant % special.getQuant();
			}
			itemTotal += rule.getPrice() * quant;
			total += itemTotal;
		}
		return total;
	}
	
}
