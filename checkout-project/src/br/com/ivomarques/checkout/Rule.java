package br.com.ivomarques.checkout;

public class Rule {
	
	private String product;
	private Double price;
	private SpecialPrice specialPrice;
	
	public Rule(String product, Double price, SpecialPrice specialPrice) {
		this.product = product;
		this.price = price;
		this.specialPrice = specialPrice;
	}

	public String getProduct() {
		return product;
	}

	public Double getPrice() {
		return price;
	}

	public SpecialPrice getSpecialPrice() {
		return specialPrice;
	}
}
