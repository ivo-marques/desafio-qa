package br.com.ivomarques.checkout;

public class Main {

	public static void main(String[] args) {
		Rule rules[] = {
				new Rule("A", 50.0, new SpecialPrice(3, 130.0))
				, new Rule("B", 30.0, new SpecialPrice(2, 45.0))
				, new Rule("C", 20.0, null)
				, new Rule("D", 15.0, null)
			};
		Checkout check = new Checkout(rules);
		check.scan("C");
		check.scan("D");
		check.scan("A");
		check.scan("A");
		System.console().printf("%.2f", check.total());
	}

}
