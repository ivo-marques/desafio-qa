package br.com.ivomarques.checkout;

public class SpecialPrice {

	private Integer quant;
	private Double price;
	
	public SpecialPrice(Integer quant, Double price) {
		this.quant = quant;
		this.price = price;
	}
	
	public Integer getQuant() {
		return quant;
	}
	public Double getPrice() {
		return price;
	}
	
}
