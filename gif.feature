#language: pt-br
Funcionalidade: Enviar GIF
     Para quando eu acessar o whatsapp para iOS
     Eu, como usuário
     Desejo enviar um GIF para um contato

Contexto: Usuário com Whatsapp para iOS
     Dado que sou usuário
     E acesso o Whatsapp
     E acesso a guia conversas

Cenário: Enviar GIF para um Contato com histórico  
     Quando eu toco em um histórico de contato
     E toco no ícone de mais
     E toco na opção "Fotos e Vídeos"
     E toco na opção GIF
     E seleciono um GIF
     E toco no ícone de enviar
     Então o GIF selecionado é enviado
     E o GIF selecionado aparece no histórico da conversa